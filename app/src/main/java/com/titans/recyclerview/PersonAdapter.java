package com.titans.recyclerview;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;

        public ViewHolder(View v) {
            super(v);
            nameTextView = (TextView) v.findViewById(R.id.person_name);
        }
    }

    private List<Person> persons;

    public PersonAdapter(List<Person> Thispersons) {
        persons = Thispersons;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View personView = inflater.inflate(R.layout.person_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(personView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PersonAdapter.ViewHolder holder, int position) {
        Person person = persons.get(position);

        TextView textView = holder.nameTextView;
        textView.setText(person.getName());
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }
}
