package com.titans.recyclerview;

import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import java.io.Serializable;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private boolean initial = true;
    private boolean tabletUIEnabled = false;
    private List<Person> personsAfterRotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        tabletUIEnabled = checkDevice();
    }

    @Override
    protected void onResume() {
        super.onResume();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (tabletUIEnabled) {
            fragmentTransaction.replace(R.id.tablet_list, new PersonList());
        } else {
            fragmentTransaction.replace(R.id.phoneLayoutContainer, new PersonList());
        }
        fragmentTransaction.commit();
    }

    private boolean checkDevice() {
        LinearLayout linearlayout = (LinearLayout) findViewById(R.id.tabletUI);
        if (linearlayout == null)
            return false;
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("initial", initial);
        outState.putSerializable("personsAfterRotate", (Serializable) personsAfterRotate);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        initial = savedInstanceState.getBoolean("initial");
        personsAfterRotate = (List<Person>) savedInstanceState.getSerializable("personsAfterRotate");
    }

    public boolean isInitial() {
        return initial;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    public void setPersonsAfterRotate(List<Person> personsAfterRotate) {
        this.personsAfterRotate = personsAfterRotate;
    }

    public List<Person> getPersonAfterRotate() {
        return personsAfterRotate;
    }

}
