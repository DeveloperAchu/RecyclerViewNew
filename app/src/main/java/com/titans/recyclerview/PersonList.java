package com.titans.recyclerview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class PersonList extends Fragment {

    ArrayList<Person> person;
    RecyclerView recyclePersons;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclePersons = (RecyclerView) getActivity().findViewById(R.id.person_list);

        if (((MainActivity) getActivity()).isInitial()) {
            person = Person.AddPersons(20);
            ((MainActivity) getActivity()).setPersonsAfterRotate(person);
            ((MainActivity) getActivity()).setInitial(false);
        } else {
            person = (ArrayList<Person>) ((MainActivity) getActivity()).getPersonAfterRotate();
        }

        PersonAdapter adapter = new PersonAdapter(person);
        recyclePersons.setAdapter(adapter);
        recyclePersons.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclePersons.setItemAnimator(new SlideInUpAnimator());
    }

}
