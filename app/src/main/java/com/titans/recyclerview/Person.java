package com.titans.recyclerview;

import java.util.ArrayList;

public class Person {

    private String name;

    public Person(String ThisName) {
        name = ThisName;
    }

    public String getName() {
        return name;
    }

    public static ArrayList<Person> AddPersons(int num){

        ArrayList<Person> persons= new ArrayList<Person>();

        for (int i=1;i<=num;i++){
            persons.add(new Person("Name "+ i));
        }

        return persons;
    }

}
